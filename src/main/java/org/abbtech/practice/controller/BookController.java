package org.abbtech.practice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.BookDTO;
import org.abbtech.practice.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@Tag(name = "Book management(for only admin roles)", description = "adding, getting and getting by id book")
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
private final BookService bookService;


    @Operation(summary = "adding books to our store")
    @PostMapping("/add")
    public ResponseEntity<BookDTO> addBook(@RequestBody BookDTO bookDTO){
        return new ResponseEntity<>(bookService.addBook(bookDTO), HttpStatus.CREATED);
    }

    @Operation(summary = "getting book")
    @GetMapping("/get")
    public ResponseEntity<List<BookDTO>> getAll(){
        return new ResponseEntity<>(bookService.getAllBooks(),HttpStatus.OK);
    }

    @Operation(summary = "getting books by id")
    @GetMapping("/get/{id}")
    public ResponseEntity<BookDTO> getById(@PathVariable UUID id){
        return new ResponseEntity<>(bookService.getById(id),HttpStatus.OK);
    }
}
