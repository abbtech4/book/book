package org.abbtech.practice.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.BookDTO;
import org.abbtech.practice.exception.BookNotFoundException;
import org.abbtech.practice.model.Book;
import org.abbtech.practice.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public BookDTO addBook(BookDTO bookDTO){
          Book book = Book.builder()
                .author(bookDTO.getAuthor())
                .title(bookDTO.getTitle())
                .price(bookDTO.getPrice())
                .build();
      bookRepository.save(book);
        return bookDTO;
    };

    public List<BookDTO> getAllBooks(){
       List<Book> bookList= bookRepository.findAll();
       if (bookList.isEmpty()){
           throw new BookNotFoundException("not any book in our store");
       }
        return bookList.stream().map(book->new BookDTO(book.getTitle(),book.getAuthor(),book.getPrice())).toList();
    }

    public BookDTO getById(UUID id){
      Book book=bookRepository.findById(id).orElseThrow(()->new BookNotFoundException("There is no such book"));
      BookDTO bookDTO = new BookDTO(book.getTitle(),book.getAuthor(),book.getPrice());
      return bookDTO;
    };

}
